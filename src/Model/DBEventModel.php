<?php
/** The Model implementation of the IMT2571 Assignment #1 MVC-example, storing
  * data in a MySQL database using PDO.
  * @author Rune Hjelsvold
  * @see http://php-html.net/tutorials/model-view-controller-in-php/
  *      The tutorial code used as basis.
  */

require_once("AbstractEventModel.php");
require_once("Event.php");
require_once("dbParam.php");

/** The Model is the class holding data about a archive of events.
  * @todo implement class functionality.
  */
class DBEventModel extends AbstractEventModel
{
    protected $db = null;

    /**
      * @param PDO $db PDO object for the database; a new one will be created if
      *                no PDO object is passed
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */

    public function __construct($db = null)
    {
        if ($db) {
            $this->db = $db;
        } else {
            // TODO
            try {

              $this->db = new PDO("mysql:host=".DB_HOST.";dbname=".DB_NAME.";charset=utf8", DB_USER, DB_PWD);

              $this->db->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
            } catch (\PDOException $e) {
              throw new PDOException($e->getMessage());
              //die();
            }

        }
    }

    /** Function returning the complete list of events in the archive. Events
      * are returned in order of id.
      * @return Event[] An array of event objects indexed and ordered by id.
      * @todo Complete the implementation using PDO and a real database.
      * @throws PDOException
      */
    public function getEventArchive()
    {
        $eventList = array();

        // TODO: Retrive events from the database and add to the list, one by one
        $hent=$this->db->query("SELECT * from event ORDER BY id");
        while ($rows=$hent->fetch(PDO::FETCH_ASSOC)){
                $newEvent = new Event(
                  $rows['title'],
                  $rows['date'],
                  $rows['description'],
                  $rows['id']
                );

                $eventList[]=$newEvent;
              }
        return $eventList;
    }

    /** Function retrieving information about a given event in the archive.
      * @param integer $id the id of the event to be retrieved
      * @return Event|null The event matching the $id exists in the archive;
      *         null otherwise.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      */
    public function getEventById($id)
    {
        $event = null;
        try {
          Event::verifyId($id);
          $getEvent = $this->db->query("SELECT * FROM event where id = $id")->fetchAll(PDO::FETCH_ASSOC);
          foreach ($getEvent as $value) {
            $event = new Event(
              $value['title'],
              $value['date'],
              $value['description'],
              $value['id']
            );
          }
          return $event;
        } catch (\PDOException $e){
          throw new PDOException($e->getMessage());
        }

    }
    /** Adds a new event to the archive.
      * @param Event $event The event to be added - the id of the event will be set after successful insertion.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
      */
    public function addEvent($event)
    {
        // TODO: Add the event to the database
        {
          try {
                $event->verify();
                $sql = $this->db->prepare ("INSERT INTO event (title, date, description)
                VALUES( ? , ? , ?)");
                $sql->bindValue(1, $event->title, PDO::PARAM_STR);
                $sql->bindValue(2, $event->date, PDO::PARAM_STR);
                $sql->bindValue(3, $event->description, PDO::PARAM_STR);
                $sql->execute();
                $event->id = $this->db->lastInsertId();
        }
        catch( PDOException $e){
          throw new PDOException($e->getMessage());
     }
      // unset($pdo);
     }
    }
    /** Modifies data related to a event in the archive.
      * @param Event $event The event data to be kept.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
      * @throws InvalidArgumentException If event data is invalid
     */
    public function modifyEvent($event)
    {
        // TODO: Modify the event in the database
        try {
          $event->verify();
          $sql=$this->db->prepare("UPDATE event SET title=?, date=?, description=? WHERE id = $event->id");
          $sql->bindValue(1, $event->title, PDO::PARAM_STR);
          $sql->bindValue(2, $event->date, PDO::PARAM_STR);
          $sql->bindValue(3, $event->description, PDO::PARAM_STR);
          $sql->execute();
          $event->id=$this->db->lastInsertId();
        } catch (PDOException $e) {
          throw new PDOException($e->getMessage());

        }

    }
    /** Deletes data related to a event from the archive.
      * @param $id integer The id of the event that should be removed from the archive.
      * @todo Implement function using PDO and a real database.
      * @throws PDOException
     */
    public function deleteEvent($id)
    {
      Event::verifyId($id);
       try{
        $sql=$this->db->query("DELETE FROM event WHERE id=$id");
        $sql->execute();
      }catch(PDOException $e){
        throw new PDOException($e->getMessage());
      }
    }
}
